# steps to be executed befor running the ansible pipeline
 1. try and ssh into your server using the following command: 
 ssh -i "~/.ssh/ernest_assesment.pem" ubuntu@public_ip_address

 2. Make changes into ansible config file
  sudo vim /home/ubuntu/.ansible.cfg
[defaults]
host_key_checking = False
private_key_file=~/.ssh/id_rsa.pem
inventory=/etc/ansible/hosts
remote_user = ubuntu

[ssh_connection]
control_path=%(directory)s/%%h-%%r
control_path_dir=~/.ansible/cp
#pipelining = True
scp_if_ssh = True

3. We need to add a Host in host file
sudo vim /etc/ansible/hosts
[ec2-instance]
192.168.2.50

